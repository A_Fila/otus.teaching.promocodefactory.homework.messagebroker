﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.Shared.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Services
{
    public  interface IPromocodeService
    {
        Task<PromoCode> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeToCustomerCreated request);
    }
}
