﻿using MassTransit;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Services;
using Otus.Teaching.Pcf.Shared.Core.Models;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Consumers
{
    public class GivePromoCodeToCustomerConsumer : IConsumer<GivePromoCodeToCustomerCreated>
    {
        readonly ILogger<GivePromoCodeToCustomerConsumer> _logger;
        readonly IPromocodeService _promocodeService;

        public GivePromoCodeToCustomerConsumer(ILogger<GivePromoCodeToCustomerConsumer> logger, 
                                               IPromocodeService promocodeService)
        {
            _logger = logger;
            _promocodeService = promocodeService;
        }
        public async Task Consume(ConsumeContext<GivePromoCodeToCustomerCreated> context)
        {
            var msg = context.Message;

            _logger.LogInformation($"Promocode '{msg.PromoCode}' consumed by {this.GetType().Name} from partnerId={msg.PartnerId}, date={msg.BeginDate}, MessageId={context.MessageId}");

            var promocode = await _promocodeService.GivePromoCodesToCustomersWithPreferenceAsync(msg);

            _logger.LogInformation($"Promocode id={promocode.Id} has been created");
           
        }
    }
}