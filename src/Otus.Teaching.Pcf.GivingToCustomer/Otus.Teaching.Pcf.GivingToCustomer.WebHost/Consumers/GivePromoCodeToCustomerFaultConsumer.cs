﻿using MassTransit;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.Shared.Core.Models;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Consumers
{
    public class GivePromoCodeToCustomerFaultConsumer : IConsumer<Fault<GivePromoCodeToCustomerCreated>>
    {
        private readonly ILogger<GivePromoCodeToCustomerFaultConsumer> _logger;
        public GivePromoCodeToCustomerFaultConsumer(ILogger<GivePromoCodeToCustomerFaultConsumer> logger)
        {
            _logger = logger;
        }
        public Task Consume(ConsumeContext<Fault<GivePromoCodeToCustomerCreated>> context)
        {
            var error = context.Message.Exceptions[0];
            _logger.LogError(error.Message);

            return Task.CompletedTask;
        }

    }    
    
}
