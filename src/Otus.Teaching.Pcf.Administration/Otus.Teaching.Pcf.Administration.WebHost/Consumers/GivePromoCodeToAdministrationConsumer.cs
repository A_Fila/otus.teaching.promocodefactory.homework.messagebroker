﻿using MassTransit;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Services;
using Otus.Teaching.Pcf.Administration.Core.Services;
using Otus.Teaching.Pcf.Shared.Core.Models;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Consumers
{
    public class GivePromoCodeToAdministrationConsumer : IConsumer<GivePromoCodeToCustomerCreated>
    {
        readonly ILogger<GivePromoCodeToAdministrationConsumer> _logger;
        private readonly IEmployeeService _employeeService;

        public GivePromoCodeToAdministrationConsumer(ILogger<GivePromoCodeToAdministrationConsumer> logger,
                                                     IEmployeeService employeeService)
        {
            _logger = logger;
            _employeeService = employeeService;
        }
        public async Task Consume(ConsumeContext<GivePromoCodeToCustomerCreated> context)
        {
            var msg = context.Message;

            _logger.LogInformation($"Promocode '{msg.PromoCode}' consumed by <{this.GetType().Name}> from partnerId={msg.PartnerId}, date={msg.BeginDate}, MessageId={context.MessageId}");
            if (msg.PartnerManagerId.HasValue)
            {
                await _employeeService.UpdateAppliedPromocodesAsync(msg.PartnerManagerId.Value);
                
                _logger.LogInformation($"AppliedPromocodes has been updated for partnerManagerId={msg.PartnerManagerId.Value}");
            }
        }
    }
}