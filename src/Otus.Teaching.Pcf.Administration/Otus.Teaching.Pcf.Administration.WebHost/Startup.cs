using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Castle.Core.Configuration;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.DataAccess;
using Otus.Teaching.Pcf.Administration.DataAccess.Data;
using Otus.Teaching.Pcf.Administration.DataAccess.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using IConfiguration = Microsoft.Extensions.Configuration.IConfiguration;
using Otus.Teaching.Pcf.Shared.Core.Settings;
using MassTransit;
using Otus.Teaching.Pcf.Administration.WebHost.Consumers;
using Otus.Teaching.Pcf.Administration.Core.Services;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Services;

namespace Otus.Teaching.Pcf.Administration.WebHost
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddMvcOptions(x=> 
                x.SuppressAsyncSuffixInActionNames = false);
            services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
            services.AddScoped<IDbInitializer, EfDbInitializer>();
            services.AddScoped<IEmployeeService, EmployeeService>();


            services.AddDbContext<DataContext>(x =>
            {
                //x.UseSqlite("Filename=PromocodeFactoryAdministrationDb.sqlite");
                x.UseNpgsql(Configuration.GetConnectionString("PromocodeFactoryAdministrationDb"));
                x.UseSnakeCaseNamingConvention();
                x.UseLazyLoadingProxies();
            });

            services.AddOpenApiDocument(options =>
            {
                options.Title = "PromoCode Factory Administration API Doc";
                options.Version = "1.0";
            });

            //rabbitMQ
            ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
            configurationBuilder.AddJsonFile("rabbitmq.json");
            IConfigurationRoot config = configurationBuilder.Build();

            var rabbitSettings = config.GetSection("RabbitMQ");

            //MassTransit
            var options = rabbitSettings.Get<RabbitMqSettings>();

            services.AddOptions<RabbitMqTransportOptions>()
                    .Configure(o =>
                    {
                        o.Host = options.HostName;
                        o.VHost = options.VirtualHost;
                        o.User = options.UserName;
                        o.Pass = options.Password;
                        o.Port = options.Port;
                    });

            services.AddMassTransit(cfg =>
            {
                //add consumers
                cfg.AddConsumer<GivePromoCodeToAdministrationConsumer>();
                //cfg.AddConsumer<CreateOrderFaultConsumer>();
                //cfg.AddReceiveEndpointObserver<ReceiveEndpointObserver>(); //��������� ������ �������

                //var assembly = typeof(Program).Assembly;
                //cfg.AddConsumers(assembly);

                cfg.UsingRabbitMq((context, cfgFact) =>
                {
                    cfgFact.ConfigureEndpoints(context);
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IDbInitializer dbInitializer)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }


            app.UseOpenApi();
            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            
            dbInitializer.InitializeDb();
        }
    }
}